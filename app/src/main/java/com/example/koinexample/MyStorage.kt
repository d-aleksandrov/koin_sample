package com.example.koinexample


interface MyStorage {
    fun hello(): String
}

class MyStorageImplementation : MyStorage {
    override fun hello() = "Hello!"
}