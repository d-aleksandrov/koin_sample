package com.example.koinexample

import org.koin.core.qualifier.named

class CircularDependency1(dependency2: Lazy<CircularDependency2>) {
    private val dependency2 by dependency2

    fun hello() = "Circle"

    companion object {
        val qualifier = named("CircularDependency1")
    }
}

class CircularDependency2(dependency1: Lazy<CircularDependency1>) {
    private val dependency1 by dependency1

    companion object {
        val qualifier = named("CircularDependency2")
    }
}