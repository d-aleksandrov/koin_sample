package com.example.koinexample

import android.app.Application
import com.example.koinexample.ui.main.MainViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.dsl.bind
import org.koin.dsl.module

class Application : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@Application)

            modules(appModule, MainViewModel.module)
        }
    }

    val appModule = module {
        single(CircularDependency1.qualifier) { lazy { CircularDependency1(get(CircularDependency2.qualifier)) } }
        single(CircularDependency2.qualifier) { lazy { CircularDependency2(get(CircularDependency1.qualifier)) } }

        single { get<Lazy<CircularDependency1>>(CircularDependency1.qualifier).value }
        single { MyStorageImplementation() } bind MyStorage::class
    }
}