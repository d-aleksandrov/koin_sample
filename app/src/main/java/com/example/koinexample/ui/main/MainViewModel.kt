package com.example.koinexample.ui.main

import androidx.lifecycle.ViewModel
import com.example.koinexample.CircularDependency1
import com.example.koinexample.MyStorage
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

class MainRepository(
    private val storage: MyStorage,
    private val circular: CircularDependency1
) {
    fun hello() = storage.hello()

    fun hello2() = circular.hello()
}

class MainViewModel(private val repository: MainRepository) : ViewModel() {
    companion object {
        val module = module {
            viewModel { MainViewModel(get()) }
            single { MainRepository(get(), get()) }
        }
    }

    init {
        val helloString = repository.hello()
        println(helloString)

        val helloString2 = repository.hello2()
        println(helloString2)
    }
}